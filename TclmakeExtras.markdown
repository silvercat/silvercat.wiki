## Convenience commands

So, for your convenience there are several other commands provided, each of which has a name starting from "p". Note that "puts", "parray" and "pwd" are standard Tcl commands.

#### pset

    pset variable args...

Gets the first argument being a variable and any further arguments as a value to set. If you pass exactly one argument, it's removed one level of list nesting. Additionally, the argument is expanded (Tcl subst command) in the current stack frame.

Example

    pset SOURCES file1.cc file2.cc

or

    pset SOURCES {
             $dir1/file1.cc
             $dir2/file1.cc
    }

The expansion of the inside variables is done internally by this command. The expansion in this particular case is done in the place of declaration.

#### pset+

    pset+ variable args...

Same as "pset", but the given value is appended to the current value of given variable (appended, that is, the args... are treated as list elements to be added to the current value of this variable being also a list).

#### pinit

    pinit variable args...

It expects that _variable_ **does not** exist - in which case if does the same as [pset]. If the variable does exist, [pinit] does nothing.

#### pget

    pget variable ?default?

If _variable_ exists, the expression [pget variable] is the same as _$variable_. If the _variable_ doesn't exist, then this function returns the _default_ argument, which is optional and defaults to an empty string (_$variable_ expression would end up with an exception). You can treat it as in Bash the following expression:

    ${variable:-some_default_value}

Additionally, if the given "variable" text contains a dot, then the variable argument is treated as dictionary access - the first partition is treated as a variable name and the rest as dictionary keys. That is, the following call:

    pget variable.rules.statement

results in returning the same as:

    dict get $variable rules statement

But again, if the above instruction would fail, due to either nonexistent _variable_ or nonexistent key path in the dictionary in _$variable_, the _default_ argument is returned.

Important is that the first element of the dotted expression can be an array-indexing expression:

    pget ns::somearray(some.dotted.index).rules.statement

If the array-indexing expression is found (the ")" character) in the name, then the first partition is until this ")" character, even if there are any dots before it - which is generally the most expected behavior.

#### phas

    phas variable

The variable is expected to return a boolean value. This call returns 1 in case when the variable **exists and contains a standard true boolean Tcl value** (either of: true, on, yes, 1). Otherwise it returns 0 (including when it doesn't exist).

#### pexpand

    pexpand value ?level?

This call simplifies the use of standard "subst" command, which takes the text with possibly enclosed references to variables, backslashes and command executions, and it "resolves" all these. The stack frame from which the variables should be tried during this process is in the "level" argument. It defaults to 2, which means that it will try to reach variables which are in the stack frame of the caller of the function, in which you'd call this one. You will rarely need to use this function in the Makefile, but it may be useful if you write your own extensions.

#### pdef

    pdef symbol value

This command actually defines a command without arguments, which should just return the given value. This really resembles the "=" operator from __Makefile__, which returns the value with expanding it every time when requested. However, you don't refer to it as $symbol, but as [symbol].

#### pdefv

    pdefv symbol args...

This command collects all args... and evaluates them by [expr] in place (in the place of using [pdefv]). If this succeeds (does not result in exception), the `symbol` is a name of a command that simply returns such evaluated value.

#### pdefx

    pdefx symbol expression

This grabs the expression as single argument and the resulting command under the `symbol` name evaluates the given `expression` in its context when it's called, then returns such evaluated expressions. Any exceptions could be propagated if occur in [expr] command.

#### pwrite

    pwrite filename contents

Opens a `filename` file for writing, then writes `contents` into it. It's a shortcut of opening, writing and closing, in which case Tcl standard functions use the open/close semantics for files.

#### pupdate

    pupdate filename contents

Checks first if the file for `filename` already exists and its contents are equal to `contents`. If so, it does nothing, otherwise does the same as `pwrite`.

#### pread

    pread filename

Opens the `filename` file for reading and returns its contents. It hides in the background the standard open/close semantics Tcl uses in standard commands for files.

#### pmap

    pmap function list

It executes `function` on every element in `list` and returns the list of so processed items. The `function` may be either a command that takes one argument, or a Tcl lambda (a list in form of `{args body}` - in this case `args` should contain only one argument name - see manpage for `apply` Tcl command for details).

#### pfind

    pfind masks... ?directory?

This function can be used to define source files indirectly by the fact that they are contained in particular directory and match particular mask. The directory is distinguished from mask by that it doesn't contain any "wildcards" (square bracket or asterisk). The mask should be a glob-style expression (see manpage for Tcl `glob` command for details). When called directly, it simply returns the list of all files that match the mask, by default in the current directory (if used in Silverball file, it should be the same directory as the script in which they are used).

#### prelativize

    prelativize path ?wd?

This function creates a relative path from the given `path` towards `wd`, which defaults to ".". If `wd` is "." then it's replaced with the value of [pwd]. The path is expected to be either absolute or relative towards [pwd]. It is **not** checked as to whether the resulting path leads to an existing file.

#### puncomment

    puncomment text

Takes an expected multi-line text and cuts off all lines where the first non-white character is #. Note that it doesn't treat comments as usual in Tcl: it doesn't remove "rest of the line" starting from __;#__, nor does it treat trailing backslash in comment lines as a line extension.

#### plist

    plist text...

If only one argument is given, it's treated as a list with elements, otherwise each argument is treated as a single list item. Every element undergoes expansion exactly the same way as by the Tcl command line interpreter, that is, it honors comments (also extended and started with __;#__), prevents expansion of {...} and preserves the list structure. If the argument spreads into multiple lines, the line division is also preserved, as well as backslash-line-extension is also honored. In particular, every line is passed to [list] command for processing.
