Why and why not?
===============

This page tries to describe several features and explains why they are done that way exactly.

1. Why Tcl?
--------------

I may try to explain it using statements like I found it best for the task, or due to its extendability, or that it's easier to rely on existing language and extend it rather than reinventing the wheel by starting from a bunch of sticks, as cmake does, for example. But I must admit even this statement might be biased. So, let me be clear on that: I like Tcl.

On the other hand, the fact that this language is easily extensible and widely adaptable is one of the most important reasons why I liked it. Not that I'm a big fan of its syntax (I am actually a C++ programmer and I happen to mistake the bracket types after if or by array indexing), I just find the fact that it's extensible most important.

Note also that I have extended this syntax to something that isn't a kind-of natural syntax support in Tcl. Parsing -option value statements and even allowing them to be spread to multiple arguments or alternatively contained in braces, it's something that is **parsed manually**. Also special syntax for statements used in Makefile's rules is parsed manually. The only thing that Tcl helps me in it is to handle the braces correctly and expand variables and commands, as well as provide utility commands.

You don't like it? I hated this language for the first half a year when I had to work with this. I changed my mind somehow when I saw how much its extendability and flexibility works for my advantage. If you don't know what I'm talking about, then tell me, how much time did you try to make extensions or enhancement to any development support tool you had to use? How much things can you express as a set of task-specific configuration, if you must align yourself to a syntax of Perl, Python or Javascript?

That's why.

2. Why -depspec cached isn't default?
--------------------------------------------------

I was considering it. The problem is that this is something specific for gcc and not every compiler supports it. May happen that some of them have to support it different way, but there's always a possibility that a compiler doesn't support them at all.

This needs that the compiler be able to generate the dependency file in one step with the object file when it compiles a source file into object file. The format is even specific for Makefile, so it was kind-of a win of practice over portability. The format is not even compatible with `Makefile.tcl` syntax - `agmake` just has ability to correctly parse it. Hard to say how the `Makefile` syntax for dependency definition is universal, however it's hard to say if anything better can be created - and this already exists.

3. Why or why not make distclean?
---------------------------------------------

The distclean target is available in autotools and so it is in Silvercat. But I found a funny section in `cmake` FAQ explaining [why they don't support any distclean](https://cmake.org/Wiki/CMake_FAQ#CMake_does_not_generate_a_.22make_distclean.22_target._Why.3F). In short: We don't support it because we cannot guarantee that it will do a complete cleanup of all project files. Just use shadow builds and you can easily fix your problem.

You know, I'm from Poland and I can assure you that this is one of the most often stupid excuse that a company pays against a customer - besides of course the best stupid excuse all times, "It's not my fault". 

There are two basic problems with this excuse in this case:

1. No one expects you to automatically find all possible generated files that the build system couldn't have had idea of. If you think that `distclean` name would suggest something like that, then use another name. Developers expect you to do only one simple thing: get rid of the garbage that **you** have polluted the user's directory with. Just simply **clean up after yourself**. Saying that you don't clean up because others still may leave some garbage reminds me of a child saying that "if my brother doesn't clean up after himself, nor will I do".
2. The Autotools that provide this feature, provide also a possibility to **declare distclean files**. Silvercat may have or have not this possibility (things might change since the time I wrote this text), but at least all targets in Silvercat may have simply **declared output**, and all output files are considered to be deleted on distclean.

That's why Silvercat does only the following:

* distclean does clean and then distclean attached to targets
* then deletes Makefile.tcl
* if you use shadow build, this should leave your directory empty - unless you have some generated untracked files

The `distclean` target doesn't guarantee you to find and delete all build-generated files, neither in Autotools nor in Silvercat. Of course, I saw somewhere kind-of distclean target that does `find . -name *.o -o -name *.a`, but I find that both stupid and dangerous (hello, Linus!).

4. Why no support for precompiled headers?
-----------------------------------------------------------

I find it slick. This is actually the problem of C and C++ languages. It's something that every compiler supports different way and they only have some common logics and intentions.

This feature may be supported only temporarily because times change and it may soon be not needed and completely deprecated:

* This feature's only use is that the source files compile faster.
* For C language the files get compiled already so fast on today used machines that the time advantage provided by precompiled headers is negligible.
* For C++ language it **may** have some observable compile time improvement (haven't observed myself any, but I believe that this has been proven worth a shot in specific situations), but:
   * The header file provides some definitions that may depend on `#define` statements in other, including user files, then also compile flags and `-D` flags passed to the compiling command. This makes them usable only in one build definition. Change one small flag, which affects only one of your source files and you start with cold cache. Not even mentioning the not always predictable changes invisible for the compiler. This is because of such a nature of header files - you really cannot reliably cache-compile them. I am also a C++ developer myself, I work sometimes with *QMake* tool from *Qt* library, and I can tell you about many situations, when **I had to manually delete the precompiled header files** because the current form causes a build break and the build system cannot otherwise recover by itself.
   * C++ in future, possibly already in C++17 version, will have modules support, which will turn it into dedicated compiler-linker (no longer using underlying C-dedicated layer). When the `#include` directive can be resolved into **generating**-alike header file out of a module file, then it will be much like precompiled header works now, except the fact that this will be now a "precompiled header and object file" in one, done portable and standardized way. This will have the advantage of the precompiled headers, without the need of any build system support (although with the need of probably reshaping the source files themselves).

At least if this has to be provided by Silvercat, it's at the bottom of the wishlist.