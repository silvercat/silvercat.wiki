The current version is in the mostly imaginable form, that is:

 - running "ag.tcl genmake" in the directory that contains a file named Makefile.ag.tcl, generates and rewrites a file named Makefile.tcl file with expected ruleset
 - currently the build definition can result in compiling C or C++ sources into a program or static library
 - additional targets with install and clear are available.
 - the program and library target types are supported. Currently library target can only result in a static library. This target type has header files in installation targets.

Both main scripts and ingredients are contained in bin/ directory. The ag.tcl script is now split into smaller parts, which start from agv.p prefix. The main goal about the script construction is that all helper functions should be moved to agv.p.utilities.tcl file, while in the ag.tcl file all directly defined procedures should be public procedures only. Currently only some of the procedures are moved.

The examples are: examples/tclmake contains the example Makefile.tcl definition for a simple case. In the examples/silvercat you can find the Makefile.ag.tcl file that demonstrates some of the features of Silvercat.

Framework definition is now the definition of a namespace. It should contain procedures with appropriate names:

 - prepare: execute before any internal processing
 - compile-pre: after the sources have been processed, but compile rules are not yet generated
 - compile-post: after all compile rules have been generated, but not yet the linker and install rules
 - complete: when all internal processing has been finished

Pending things:

* create processing dynamic libraries (not only static)
* the static library definition should be defined as "devel", while the dynamic library should be considered "runtime". Note that for Windows the devel package should include both static library and the static-dynamic-binder library.
* allow for general directories for build intermediate files (support shadow builds)
* support subdirectories (continue using the same profile, update the database from the ag files lying in subdirectories, but using directory namespace for targets)
* support custom type targets, which are managed entirely by frameworks
